# medieval-warfare
A strategy game like a fusion of chess and Age Of Empires.


Battles have been waged between forces from time immemorial, when the stone age had passed, to when the first empires were forged, to when gunpowder was created, to when World War 2 ended, to even today. The point of a battle or war is to finally defeat the enemy forces by annahilating their numbers. But the most important element in that process does not wind up to sheer numbers, or the technological powress, or the amount of resources that you control. It is how you use all of those factors in you battle plan to win the war. In the end, it lies in your skill, and your logic behind the actions you take. In the end, it is pure strategy.

This game involves using your tactical powress and strategical skills by using the most basic elements of war, like few classes of units with different abilities, limited resources, slow collection of resources, and of course, a 'base', which is where your command center lies. From here, you control your forces, or send in more troops, just like one would do in actual war from their headquarters.

The war ends when any one side annihilates the opposing force by destroying the enemy base. After this, the defeated force has no hope of reconcilating or retaliating.



Actual Gameplay:

So, now that the basic ideology of the game has been explained, the actual gameplay can now be satisfactorily explained.

The map consists of a 14x14 square board, much like an extended chess board. Each square is called a tile, which has several properties.

In two opposite corners, are the two 'bases' of both sides. This game is a two-player game, wherein two human players sit in front of the same computer, and virtually fight against each other. Player one controls the base in the top-left corner, and Player two controls the base in the bottom right corner.

In real life, there are various classes of units with differing skills and abilities, such as archers, that have a long range... tanks, that have huge firepower... and even normal soldiers, that are normal forces in comparison to others. Similarily, this game includes three classes, namely, cavalry, infantry, and scouts. Cavalry are powerful, but slow. Infantry are slow too, but have a good amount of damage capacity. Scouts are weak, but posess incredible mobility, and do not cost much.

Each class has its own statistics. Attack statistics denote how much damage a unit can inflict on an enemy unit. Health statistics denote how much damage a unit can take. And range statistics denote how far a unit can move in one turn. The actual statistics are given as follows...

                  | Attack | Health | Range |
         ------------------------------------
         Cavalry  |   4         9       1
         Infantry |   3         6       2
         Scout    |   2         3       2

But none of these units come in free of cost. Each unit has its own price. As the game progresses, the player will accumulate resources by moving units around the board, and collect randomly scattered resources in the form of food and production. A unit can only be created in tiles neighboring the player's base. To create a unit, the player must sacrifice some of these resources, in place for a brand new unit. A unit cannot be created without sufficient resources. The prices are given as follows...

                  |   Food  | Production |
         ---------------------------------
         Cavalry  |     20         20
         Infantry |     14         14
         Scout    |      8          8

The entire game is turn-based... which basically means that after a player has made a move, or has created a unit, it is the next player's turn to make a move, or create a unit, after which the turns keep interchanging.

When the game begins, both players are given a free scout to begin with. Each side must use these scouts to roam around and collect resources in order to create more units, and in time, a sizeable army. A move/creation can be made by double clicking on the option in the menu in the right side of the board. If an option to create a particular unit is double-clicked, then the player is required to double click on the target tile in which the unit is to be created. Be warned that the target tile must be adjacent to the base. If all adjacent tiles are filled, then the player must make space by moving those units away. And the player must have sufficient resources to create whichever unit is required. If an option to move a unit is double-clicked, the player must double-click on his unit that he wants to move, and then double-click on the target tile. Be warned that the target tile must be within range of the unit.

To attack an enemy unit, the player has to select a friendly unit, and move it to a tile occupied by an enemy unit. Then, the units will do battle, and based on their respective health and attack values, the outcome of that fight is decided.

To attack the enemy base, a player must move a friendly unit onto any tile of the enemy base. However, the base will not be easy to destroy. It will have an incredibly high health value, and will retaliate with a minor attack to any unit that attacks it.

The game is won when either player destroys all four tiles of the enemy base.

So, let strategy take over, and push your way to glory!

For any suggestions, bug reports, or potential improvements, feel free to mail in your comments to soorya.annadurai@gmail.com , and i will be sure to have a look at them!

Credits:
First, to the game "Sid Meier's Civilization V", for providing the initial spark for the idea.
Second and third, to my friends Vibhav S. Kalaparthy and Allen D. Sunny, for helping me create the paper-ed version of the game.
Fourth, to my dad, whose suggestions and modifications were vital in several stages of programming.
Fifth, to my mom, who would constantly review my progress, and make valuable suggestions for its improvement.
And most importantly, to all my friends in school, whose suggestions were vital in shaping the game itself.
And then of course, me! I did do all the tedious labor of programming it, so I assumed i would get some of the credit too...!

Thanks a lot! Mail me with your comments!


## Runtime instructions
You have to resize the command prompt screen before playing the game!

To do this,
1. Open the game (the .exe file).
2. Right-click on the menu bar (the bar on the top of the window, displaying the file directory of the game).
3. Click on the Defaults option.
4. Go to the Layout tab.
5. Under the Window Size box (second box, vertically down), reset width to 160 and height to 70.
6. Click OK.
7. Close the game.
8. Open the game again.

And we're done!

And, to select any option in the game, you have to double-click the option.

(Old Google Code archive: https://code.google.com/archive/p/medieval-warfare/downloads)
