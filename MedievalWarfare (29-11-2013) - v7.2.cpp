#include <iostream>
#include <windows.h>

//Define extra colours
#define FOREGROUND_WHITE		(FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN)
#define FOREGROUND_YELLOW       	(FOREGROUND_RED | FOREGROUND_GREEN)
#define FOREGROUND_CYAN		        (FOREGROUND_BLUE | FOREGROUND_GREEN)
#define FOREGROUND_MAGENTA	        (FOREGROUND_RED | FOREGROUND_BLUE)
#define FOREGROUND_BLACK		0
#define FOREGROUND_BROWN            (FOREGROUND_GREEN | FOREGROUND_RED)

#define FOREGROUND_INTENSE_RED		(FOREGROUND_RED | FOREGROUND_INTENSITY)
#define FOREGROUND_INTENSE_GREEN	(FOREGROUND_GREEN | FOREGROUND_INTENSITY)
#define FOREGROUND_INTENSE_BLUE		(FOREGROUND_BLUE | FOREGROUND_INTENSITY)
#define FOREGROUND_INTENSE_WHITE	(FOREGROUND_WHITE | FOREGROUND_INTENSITY)
#define FOREGROUND_INTENSE_YELLOW	(FOREGROUND_YELLOW | FOREGROUND_INTENSITY)
#define FOREGROUND_INTENSE_CYAN		(FOREGROUND_CYAN | FOREGROUND_INTENSITY)
#define FOREGROUND_INTENSE_MAGENTA	(FOREGROUND_MAGENTA | FOREGROUND_INTENSITY)
#define FOREGROUND_INTENSE_BROWN    (FOREGROUND_BROWN | FOREGROUND_INTENSITY)

#define BACKGROUND_WHITE		(BACKGROUND_RED | BACKGROUND_BLUE | BACKGROUND_GREEN)
#define BACKGROUND_YELLOW	        (BACKGROUND_RED | BACKGROUND_GREEN)
#define BACKGROUND_CYAN		        (BACKGROUND_BLUE | BACKGROUND_GREEN)
#define BACKGROUND_MAGENTA	        (BACKGROUND_RED | BACKGROUND_BLUE)
#define BACKGROUND_BLACK		0

#define BACKGROUND_INTENSE_RED		(BACKGROUND_RED | BACKGROUND_INTENSITY)
#define BACKGROUND_INTENSE_GREEN	(BACKGROUND_GREEN | BACKGROUND_INTENSITY)
#define BACKGROUND_INTENSE_BLUE		(BACKGROUND_BLUE | BACKGROUND_INTENSITY)
#define BACKGROUND_INTENSE_WHITE	(BACKGROUND_WHITE | BACKGROUND_INTENSITY)
#define BACKGROUND_INTENSE_YELLOW	(BACKGROUND_YELLOW | BACKGROUND_INTENSITY)
#define BACKGROUND_INTENSE_CYAN		(BACKGROUND_CYAN | BACKGROUND_INTENSITY)
#define BACKGROUND_INTENSE_MAGENTA	(BACKGROUND_MAGENTA | BACKGROUND_INTENSITY)
/*
    Black = 0,
14
    Blue =   FOREGROUND_BLUE,
15
    Green =  FOREGROUND_GREEN,
16
    Red =    FOREGROUND_RED,
17
    Cyan =  FOREGROUND_GREEN | FOREGROUND_BLUE,
18
    Purple = FOREGROUND_RED | FOREGROUND_BLUE,
19
    Orange = FOREGROUND_RED | FOREGROUND_GREEN,
20
    Yellow = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY,
21
    Grey =   FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE,
22
    White =  FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
*/

using namespace std;



const int scout_price = 4;
const int infantry_price = 6;
const int cavalry_price = 9;
const int base_price = 50;

const int scout_attack = 2;
const int infantry_attack = 3;
const int cavalry_attack = 4;
const int base_attack = 2;

const int scout_range = 2;
const int infantry_range = 2;
const int cavalry_range = 1;
const int base_range = 0;

const int scout_health = 3;
const int infantry_health = 6;
const int cavalry_health = 9;
const int base_health = 15;



const char piece_type[] = {' ', 'c', 'i', 's', 'b', 'C', 'I', 'S', 'B'};
const int attack[] = {0, cavalry_attack, infantry_attack, scout_attack, base_attack, cavalry_attack, infantry_attack, scout_attack, base_attack};
const int start_health[] = {0, cavalry_health, infantry_health, scout_health, base_health, cavalry_health, infantry_health, scout_health, base_health};
const int piece_range[] = {0, cavalry_range, infantry_range, scout_range, base_range, cavalry_range, infantry_range, scout_range, base_range};
static int piece_count[] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
const int food_price[] = {0, cavalry_price, infantry_price, scout_price, base_price, cavalry_price, infantry_price, scout_price, base_price};
const int prod_price[] = {0, cavalry_price, infantry_price, scout_price, base_price, cavalry_price, infantry_price, scout_price, base_price};
const int my = 1;
const int his = 2;
const int my_cavalry = 1;
const int my_infantry = 2;
const int my_scout = 3;
const int my_base = 4;
const int his_cavalry = 5;
const int his_infantry = 6;
const int his_scout = 7;
const int his_base = 8;
const int att_food = 0;
const int att_prod = 1;
const int att_present = 2;
const int att_count = 3;
const int att_health = 4;
const int att_attack = 5;
const int att_range = 6;
const int att_creation = 7;
static int my_food_count = 4000;
static int his_food_count = 4000;
static int my_prod_count = 4000;
static int his_prod_count = 4000;
static int mouse_x, mouse_y, mouse_X, mouse_Y, mouse_ctrl_x, mouse_ctrl_y, turn_count = 0;
int board[15][15][8]; //row, column, attributes [food & production & unit present & unit count & health & attack & range & creation]. Forces all dimensions to have garbage value at 0, except attributes.

void gotoXY(int x, int y);
void mouse_pos(int &mouse_x, int &mouse_y);
void hidecursor();
int tile_finder_x(int a);
int tile_finder_y(int b);
void display_text_xy(int a, int b, string text);
void mark_piece(int a, int b);
void mark_piece_health(int a, int b);
void paint_piece(int a, int b);
void erase_tile(int a, int b);
void erase_piece(int a, int b);
void set_board();
void resource_creator();
void resource_tally(int a, int b);
char mouse_ctrl(int &code);
void create_new_piece(int a, int b, int code); //for user.
void create_new_piece_coordinates(int &a, int &b, int code, int &space);
void move_ranged_piece(int a, int b, int A, int B); //for user.
int range_check(int a, int b, int A, int B);
void attack_piece(int a, int b, int A, int B);
void kill_piece(int a, int b);
int turn_check();
void clr_options(int who);
int my_victory_check();
int his_victory_check();


    HANDLE hstdin  = GetStdHandle( STD_INPUT_HANDLE  );
	HANDLE hstdout = GetStdHandle( STD_OUTPUT_HANDLE );
	WORD   index   = 0;

	// Remember how things were when we started
	CONSOLE_SCREEN_BUFFER_INFO csbi;

int main ()
{

	GetConsoleScreenBufferInfo( hstdout, &csbi );

    /*
    system("mode 140,70");   //Set mode to ensure window does not exceed buffer size
    SMALL_RECT WinRect = {0, 0, 140, 70};   //New dimensions for window in 8x12 pixel chars
    SMALL_RECT* WinSize = &WinRect;
    SetConsoleWindowInfo(GetStdHandle(STD_OUTPUT_HANDLE), true, WinSize);   //Set new size for window
    */
    hidecursor();
    display_text_xy(91,6,"Medieval Warfare");
    display_text_xy(90,7,"==================");
    gotoXY (0,2);
    set_board();
    display_text_xy(85,57,"Double-click anywhere to begin!");
    mouse_pos(mouse_x,mouse_y);
    display_text_xy(85,57,"                               ");

    display_text_xy(91,13,"Piece Description:");
    display_text_xy(91,14,"-----------------------");
    display_text_xy(91,15,"| {Unit attack value} |");
    display_text_xy(91,16,"|Piece type/Unit count|");
    display_text_xy(91,17,"| [Unit health value] |");
    display_text_xy(91,18,"-----------------------");

    /*
    display_text_xy(91,22,"Unit prices...");

    gotoXY(85,24);
    cout << "Cavalry  ->  " << cavalry_price << " food, " << cavalry_price << " prod, " << cavalry_attack << " attack, " << cavalry_health << " health, " << cavalry_range << " range";
    gotoXY(85,26);
    cout << "Infantry ->  " << infantry_price << " food, ";
    //gotoXY(104,27);
    cout << infantry_price << " prod";
    gotoXY(85,28);
    cout << "Scout    ->  " << scout_price << " food, ";
    //gotoXY(104,29);
    cout << scout_price << " prod";
    */
    display_text_xy(80, 22, "Unit statistics.");
    display_text_xy(80, 23, "----------------");


    display_text_xy(80, 25, "Cavalry:");

    gotoXY(80,26);
    cout << "Food price: " << cavalry_price;

    gotoXY(80,27);
    cout << "Prod price: " << cavalry_price;

    gotoXY(80,28);
    cout << "Attack:     " << cavalry_attack;

    gotoXY(80,29);
    cout << "Health:     " << cavalry_health;

    gotoXY(80,30);
    cout << "Range:      " << cavalry_range;


    display_text_xy(99, 25, "Infantry:");

    gotoXY(99,26);
    cout << "Food price: " << infantry_price;

    gotoXY(99,27);
    cout << "Prod price: " << infantry_price;

    gotoXY(99,28);
    cout << "Attack:     " << infantry_attack;

    gotoXY(99,29);
    cout << "Health:     " << infantry_health;

    gotoXY(99,30);
    cout << "Range:      " << infantry_range;


    display_text_xy(118, 25, "Scout:");

    gotoXY(118,26);
    cout << "Food price: " << scout_price;

    gotoXY(118,27);
    cout << "Prod price: " << scout_price;

    gotoXY(118,28);
    cout << "Attack:     " << scout_attack;

    gotoXY(118,29);
    cout << "Health:     " << scout_health;

    gotoXY(118,30);
    cout << "Range:      " << scout_range;

    create_new_piece(1,1,my_base);
    create_new_piece(1,2,my_base);
    create_new_piece(2,1,my_base);
    create_new_piece(2,2,my_base);
    create_new_piece(14,14,his_base);
    create_new_piece(14,13,his_base);
    create_new_piece(13,14,his_base);
    create_new_piece(13,13,his_base);
    board[1][1][att_health] = 10;
    board[1][1][att_attack] = 0;
    board[14][14][att_health] = 10;
    board[14][14][att_attack] = 0;
    paint_piece(1,1);
    paint_piece(14,14);

    my_food_count = 50;
    his_food_count = 50;
    my_prod_count = 50;
    his_prod_count = 50;

    bool exit_status = FALSE;

    create_new_piece(3,3,my_scout);
    create_new_piece(12,12,his_scout);

    while (my_victory_check() == 0 && his_victory_check() == 0 && exit_status == FALSE)
    {
        static int code;
        switch (mouse_ctrl(code))
        {
               case 'x':
                    {
                        display_text_xy(85,57,"Exiting...");
                        Sleep(1000);
                        exit_status = TRUE;
                        break;
                    }
               case 'c':
                    {
                        int a, b, space;
                        if (turn_check() == my)
                        {
                                                if (code >= 4)
                                                {
                                                         display_text_xy(85,57,"Cannot create this piece.");
                                                         Sleep(1000);
                                                         display_text_xy(85,57,"                         ");
                                                }
                                                else
                                                {
                                                         create_new_piece_coordinates(a, b, code, space);
                                                         if (space == 1)
                                                         {
                                                                   create_new_piece(a, b, code);
                                                         }
                                                }
                        }
                        else if (turn_check() == his)
                        {
                                                 if (code <= 3)
                                                 {
                                                         display_text_xy(85,57,"Cannot create this piece.");
                                                         Sleep(1000);
                                                         display_text_xy(85,57,"                         ");
                                                }
                                                else
                                                {
                                                         create_new_piece_coordinates(a, b, code, space);
                                                         if (space == 1)
                                                         {
                                                                   create_new_piece(a, b, code);
                                                         }
                                                }
                        }
                        Sleep(100);
                        exit_status = FALSE;
                        break;
                    }
                    case 'm':
                    {
                        mouse_pos(mouse_x, mouse_y);
                        mouse_pos(mouse_X, mouse_Y);
                        if (turn_check() == my)
                        {
                           if (board[tile_finder_x(mouse_x)][tile_finder_y(mouse_y)][att_present] <= 3)
                           {
                               move_ranged_piece(tile_finder_x(mouse_x),tile_finder_y(mouse_y),tile_finder_x(mouse_X),tile_finder_y(mouse_Y));
                           }
                           else
                           {
                               display_text_xy(85,57,"Cannot move this piece.");
                               Sleep(1000);
                               display_text_xy(85,57,"                       ");
                           }
                        }
                        else if (turn_check() == his)
                        {
                             if (board[tile_finder_x(mouse_x)][tile_finder_y(mouse_y)][att_present] >= 5)
                             {
                                move_ranged_piece(tile_finder_x(mouse_x),tile_finder_y(mouse_y),tile_finder_x(mouse_X),tile_finder_y(mouse_Y));
                             }
                             else
                             {
                                 display_text_xy(85,57,"Cannot move this piece.");
                                 Sleep(1000);
                                 display_text_xy(85,57,"                       ");
                             }
                        }
                        Sleep(100);
                        exit_status = FALSE;
                        break;
                    }
        }
        if (my_victory_check() == 1)
        {
                               display_text_xy(85,57,"Player 1 wins the game!");
                               Sleep(2000);
                               display_text_xy(85,57,"                       ");
                               break;
        }
        if (his_victory_check() == 2)
        {
                               display_text_xy(85,57,"Player 2 wins the game!");
                               Sleep(2000);
                               display_text_xy(85,57,"                       ");
                               break;
        }
    }
    return 0;
}
////////////////////////////////////////////////////////
void gotoXY(int x, int y)
{
     COORD CursorPosition;
     CursorPosition.X = x; // Locates column
     CursorPosition.Y = y; // Locates Row
     SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),CursorPosition); // Sets position for next thing to be printed
}

void mouse_pos(int &mouse_x, int &mouse_y)
{
    HANDLE hIn = GetStdHandle(STD_INPUT_HANDLE);
    bool Continue = TRUE;
    DWORD EventCount;
    INPUT_RECORD InRec;
    DWORD NumRead;
    while (Continue)
    {
            Sleep(10);
            ReadConsoleInput(hIn,&InRec,1,&NumRead);
            if (InRec.Event.MouseEvent.dwEventFlags == DOUBLE_CLICK)
            {
                 mouse_x = InRec.Event.MouseEvent.dwMousePosition.X;
                 mouse_y = InRec.Event.MouseEvent.dwMousePosition.Y;
                 Continue = FALSE;
            }
    }
}

void hidecursor()
{
    HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_CURSOR_INFO info;
    info.dwSize = 100;
    info.bVisible = FALSE;
    SetConsoleCursorInfo(consoleHandle, &info);
}

int tile_finder_x(int a)
{
    switch ((a - 6) % 5)
    {
           case 0: return (((a-6) / 5) + 1);
           case 1: return (((a-7) / 5) + 1);
           case 2: return (((a-8) / 5) + 1);
           case 4: return (((a-5) / 5) + 1);
    }
}

int tile_finder_y(int b)
{
    switch ((b - 4) % 4)
    {
           case 0: return (((b-4) / 4) + 1);
           case 1: return (((b-5) / 4) + 1);
           case 3: return (((b-3) / 4) + 1);
    }
}

void display_text_xy(int a, int b, string text)
{
     gotoXY(a,b);
     cout << text;
}

void mark_piece(int a, int b)
{
     gotoXY (((a-1) * 5) + 6, ((b-1) * 4) + 4);
}

void mark_piece_health(int a, int b)
{
     gotoXY (((a-1) * 5) + 5, ((b-1) * 4) + 5);
}

void paint_piece(int a, int b)
{

     if (board[a][b][att_present] <= 4)
     {
         SetConsoleTextAttribute( hstdout, FOREGROUND_INTENSE_RED | BACKGROUND_INTENSE_YELLOW ); //Set player 1's colors.
     }
     else if (board[a][b][att_present] >= 5)
     {
         SetConsoleTextAttribute( hstdout, FOREGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_INTENSITY ); //Set player 2's colors.
     }
     gotoXY (((a-1) * 5) + 5, ((b-1) * 4) + 4); // piece display.
     cout << " " << piece_type[board[a][b][att_present]] << board[a][b][att_count];
     if (board[a][b][att_count] <= 9)
     {
         cout << " "; //Fill in the tile.
     }
     gotoXY (((a-1) * 5) + 5, ((b-1) * 4) + 5); // health display.
     cout << "[" << board[a][b][att_health] << "]";
     if (board[a][b][att_health] <= 9)
     {
         cout << " "; //Fill in the tile.
     }
     gotoXY (((a-1) * 5) + 5, ((b-1) * 4) + 3); // attack display.
     cout << "{" << board[a][b][att_attack] << "}";
     if (board[a][b][att_health] <= 9)
     {
         cout << " "; //Fill in the tile.
     }

     SetConsoleTextAttribute( hstdout, csbi.wAttributes );
}

void erase_tile(int a, int b)
{
     display_text_xy(((a-1)*5)+5,((b-1)*4)+3,"    ");
     display_text_xy(((a-1)*5)+5,((b-1)*4)+4,"    ");
     display_text_xy(((a-1)*5)+5,((b-1)*4)+5,"    ");
}

void erase_piece(int a, int b)
{
     for (int i = 2; i <= 6; i++)
     board[a][b][i] = 0; //loops through attributes present, health, attack, range.
     erase_tile(a,b);
}

void set_board()
{
     cout << "    -----------------------------------------------------------------------" << endl;
     for (int down = 0; down < 14; down++)
     {
         for (int straight = 0; straight < 3; straight++)
             if (down == 0 || down == 1)
                  cout << "    |!!!!|!!!!|    |    |    |    |    |    |    |    |    |    |    |    |" << endl;
             else if (down == 12 || down == 13)
                  cout << "    |    |    |    |    |    |    |    |    |    |    |    |    |****|****|" << endl;
             else
                  cout << "    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |" << endl;
         cout << "    -----------------------------------------------------------------------" << endl;
     }
     for (int i = 1; i <= 14; i++)
         for (int j = 1; j <= 14; j++)
             for (int k = 1; k <= 7; k++) //loops through all attributes.
                 board[i][j][k] = 0;
}

void resource_creator()
{
    srand((unsigned) time (NULL));
	for(int i=1; i <= 14; i++)    //This loops on the rows.
		for(int j=1; j <= 14; j++) //This loops on the columns
              for(int k=0; k<2; k++) //This loops on food and production.
                    board[i][j][k] = rand() % 3;
}

void resource_tally(int a, int b)
{
     resource_creator();
     if (board[a][b][att_present] <= 3)
     {
                                  my_food_count += board[a][b][att_food];
                                  my_prod_count += board[a][b][att_prod];
                                  display_text_xy(80,43,"resources");
                                  display_text_xy(80,44,"---------");
                                  gotoXY(81,45);
                                  cout << "Food = " << my_food_count << "     ";
                                  gotoXY(81,46);
                                  cout << "Prod = " << my_prod_count << "     ";
     }
     else if (board[a][b][att_present] >= 5)
     {
                                  his_food_count += board[a][b][att_food];
                                  his_prod_count += board[a][b][att_prod];
                                  display_text_xy(103,43,"RESOURCES");
                                  display_text_xy(103,44,"---------");
                                  gotoXY(104,45);
                                  cout << "Food = " << his_food_count << "     ";
                                  gotoXY(104,46);
                                  cout << "Prod = " << his_prod_count << "     ";
     }
}

char mouse_ctrl(int &code)
{
     display_text_xy(115,57,"Exit");
     display_text_xy(115,58,"----");
     //SetConsoleTextAttribute( hstdout, FOREGROUND_RED | FOREGROUND_INTENSITY | BACKGROUND_BLUE ); //Set player 1's colors.
     display_text_xy(80,32,"player 1:");
     //SetConsoleTextAttribute( hstdout, FOREGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_INTENSITY ); //Set player 2's colors.
     display_text_xy(103,32,"PLAYER 2:");
     SetConsoleTextAttribute( hstdout, csbi.wAttributes );
     if (turn_check() == my)
     {
                      display_text_xy(80,35,"Create cavalry");
                      display_text_xy(80,36,"Create infantry");
                      display_text_xy(80,37,"Create scout");
                      display_text_xy(80,38,"Move unit");
                      clr_options(his);
                      mouse_pos(mouse_ctrl_x,mouse_ctrl_y);
                      clr_options(my);
                      if (mouse_ctrl_y == 57 && mouse_ctrl_x >= 115 && mouse_ctrl_x <= 118)
                      {
                                    code = 0;
                                    return 'x';
                      }
                      if (mouse_ctrl_y == 35 && mouse_ctrl_x >= 80 && mouse_ctrl_x <= 93)
                      {
                                    display_text_xy(80,35,"Create cavalry");
                                    code = 1;
                                    return 'c';
                      }
                      if (mouse_ctrl_y == 36 && mouse_ctrl_x >= 80 && mouse_ctrl_x <= 94)
                      {
                                    display_text_xy(80,36,"Create infantry");
                                    code = 2;
                                    return 'c';
                      }
                      if (mouse_ctrl_y == 37 && mouse_ctrl_x >= 80 && mouse_ctrl_x <= 91)
                      {
                                    display_text_xy(80,37,"Create scout");
                                    code = 3;
                                    return 'c';
                      }
                      if (mouse_ctrl_y == 38 && mouse_ctrl_x >= 80 && mouse_ctrl_x <= 88)
                      {
                                    display_text_xy(80,38,"Move unit");
                                    code = 0;
                                    return 'm';
                      }
     }
     else if (turn_check() == his)
     {
                      clr_options(my);
                      display_text_xy(103,35,"Create cavalry");
                      display_text_xy(103,36,"Create infantry");
                      display_text_xy(103,37,"Create scout");
                      display_text_xy(103,38,"Move unit");
                      mouse_pos(mouse_ctrl_x,mouse_ctrl_y);
                      clr_options(his);
                      if (mouse_ctrl_y == 57 && mouse_ctrl_x >= 115 && mouse_ctrl_x <= 118)
                      {
                                       code = 0;
                                       return 'x';
                      }
                      if (mouse_ctrl_y == 35 && mouse_ctrl_x >= 103 && mouse_ctrl_x <= 116)
                      {
                                       display_text_xy(103,35,"Create cavalry");
                                       code = 5;
                                       return 'c';
                      }
                      if (mouse_ctrl_y == 36 && mouse_ctrl_x >= 103 && mouse_ctrl_x <= 117)
                      {
                                       display_text_xy(103,36,"Create infantry");
                                       code = 6;
                                       return 'c';
                      }
                      if (mouse_ctrl_y == 37 && mouse_ctrl_x >= 103 && mouse_ctrl_x <= 114)
                      {
                                       display_text_xy(103,37,"Create scout");
                                       code = 7;
                                       return 'c';
                      }
                      if (mouse_ctrl_y == 38 && mouse_ctrl_x >= 103 && mouse_ctrl_x <= 111)
                      {
                                       display_text_xy(103,38,"Move unit");
                                       code = 0;
                                       return 'm';
                      }
     }
}

void create_new_piece(int a, int b, int code)
{
     if (board[a][b][att_present] == 0)
     {
           if ((code <= 4 && food_price[code] <= my_food_count && prod_price[code] <= my_prod_count) || (code >= 5 && food_price[code] <= his_food_count && prod_price[code] <= his_prod_count))
           {
                     piece_count[code]++;
                     board[a][b][att_present] = code;
                     board[a][b][att_count] = piece_count[code];
                     board[a][b][att_health] = start_health[code];
                     board[a][b][att_attack] = attack[code];
                     board[a][b][att_range] = piece_range[code];
                     if (code <= 4)
                     {
                              my_food_count -= food_price[code];
                              my_prod_count -= prod_price[code];
                     }
                     else
                     if (code >= 5)
                     {
                              his_food_count -= food_price[code];
                              his_prod_count -= prod_price[code];
                     }
                     paint_piece(a,b);
                     resource_tally(a,b);
                     turn_count++;
           }
           else
           {
                     display_text_xy(85,57,"Insufficient resources!");
                     Sleep(1000);
                     display_text_xy(85,57,"                       ");
           }
     }
}

void create_new_piece_coordinates(int &a, int &b, int code, int &space)
{
     space = 0;
//     a = 0;
 //    b = 0;
     for (int i = 1; i <= 14; i++)
     for (int j = 1; j <= 14; j++)
         board[i][j][att_creation] = 0;

     for (int i = 1; i <= 14; i++)
     for (int j = 1; j <= 14; j++)
     if ((code <= 3) && (board[i][j][att_present] == 4))
     {
               if (board[i+1][j][att_present] != 4)
                           board[i+1][j][att_creation] = 1;
               if (board[i+1][j+1][att_present] != 4)
                           board[i+1][j+1][att_creation] = 1;
               if (board[i][j+1][att_present] != 4)
                           board[i][j+1][att_creation] = 1;
               if (board[i-1][j+1][att_present] != 4)
                           board[i-1][j+1][att_creation] = 1;
               if (board[i-1][j][att_present] != 4)
                           board[i-1][j][att_creation] = 1;
               if (board[i-1][j-1][att_present] != 4)
                           board[i-1][j-1][att_creation] = 1;
               if (board[i][j-1][att_present] != 4)
                           board[i][j-1][att_creation] = 1;
               if (board[i+1][j-1][att_present] != 4)
                           board[i+1][j-1][att_creation] = 1;
     }
     else if ((code >= 5) && (board[i][j][att_present] == 8))
     {
               if (board[i+1][j][att_present] != 8)
                           board[i+1][j][att_creation] = 2;
               if (board[i+1][j+1][att_present] != 8)
                           board[i+1][j+1][att_creation] = 2;
               if (board[i][j+1][att_present] != 8)
                           board[i][j+1][att_creation] = 2;
               if (board[i-1][j+1][att_present] != 8)
                           board[i-1][j+1][att_creation] = 2;
               if (board[i-1][j][att_present] != 8)
                           board[i-1][j][att_creation] = 2;
               if (board[i-1][j-1][att_present] != 8)
                           board[i-1][j-1][att_creation] = 2;
               if (board[i][j-1][att_present] != 8)
                           board[i][j-1][att_creation] = 2;
               if (board[i+1][j-1][att_present] != 8)
                           board[i+1][j-1][att_creation] = 2;
     }
     mouse_pos(mouse_x,mouse_y);
     {
         if (code <= 3)
         {
                  if (board[tile_finder_x(mouse_x)][tile_finder_y(mouse_y)][att_creation] == 1 && board[tile_finder_x(mouse_x)][tile_finder_y(mouse_y)][att_present] == 0)
                  {
                                                a = tile_finder_x(mouse_x);
                                                b = tile_finder_y(mouse_y);
                                                space = 1;
                  }
         }
         else if (code >= 5)
         {
                  if (board[tile_finder_x(mouse_x)][tile_finder_y(mouse_y)][att_creation] == 2 && board[tile_finder_x(mouse_x)][tile_finder_y(mouse_y)][att_present] == 0)
                  {
                                                a = tile_finder_x(mouse_x);
                                                b = tile_finder_y(mouse_y);
                                                space = 1;
                  }
         }
     }
     if (space != 1)
     {
        space = 0;
        display_text_xy(85,57,"Cannot create this unit!");
        Sleep(1000);
        display_text_xy(85,57,"                        ");
     }
}

void move_ranged_piece(int a, int b, int A, int B)
{
     if (range_check(a, b, A, B) == 1)
     {
                             if (board[a][b][att_present] != 0)
                             {
                                if (board[A][B][att_present] == 0)
                                {
                                   board[A][B][att_present] = board[a][b][att_present];
                                   board[A][B][att_count] = board[a][b][att_count];
                                   board[A][B][att_health] = board[a][b][att_health];
                                   board[A][B][att_attack] = board[a][b][att_attack];
                                   board[A][B][att_range] = board[a][b][att_range];
                                   erase_piece(a,b);
                                   paint_piece(A,B);
                                   resource_tally(A,B);
                                   turn_count++;
                                }
                                else
                                {
                                    attack_piece(a,b,A,B);
                                }
                             }
                             else
                             {
                                 display_text_xy(85,57,"No piece to move!");
                                 Sleep(1000);
                                 display_text_xy(85,57,"                 ");
                             }
     }
     else if (range_check(a, b, A, B) == 0)
     {
                        display_text_xy(85,57,"Target out of range!");
                        Sleep(1000);
                        display_text_xy(85,57,"                    ");
     }
}

int range_check(int a, int b, int A, int B)
{
    if ( (a - board[a][b][att_range]) <= A && (a + board[a][b][att_range]) >= A && (b - board[a][b][att_range]) <= B && (b + board[a][b][att_range]) >= B)
         return 1;
    else
         return 0;
}

void attack_piece(int a, int b, int A, int B) //a,b -> attacking, A,B -> defending
{
     if ((board[a][b][att_present] <= 4 && board[A][B][att_present] >= 5) ||
         (board[A][B][att_present] <= 4 && board[a][b][att_present] >= 5)) //changed 1st 3 to 4.
     {
          int change_count_a = 0, change_count_A = 0;
          if ((board[a][b][att_health] > board[A][B][att_attack] / 2) &&
              (board[A][B][att_health] > board[a][b][att_attack])) //both survive.
          {
                board[a][b][att_health] -= board[A][B][att_attack] / 2;
                board[A][B][att_health] -= board[a][b][att_attack];
                paint_piece(a,b);
                paint_piece(A,B);
                resource_tally(a,b);
                resource_tally(A,B);
          }
          else if ((board[a][b][att_health] <= board[A][B][att_attack] / 2) &&
                   (board[A][B][att_health] <= board[a][b][att_attack])) //both die.
          {
                for (int i = 1; i <= 14; i++)
                for (int j = 1; j <= 14; j++)
                {
                    if (board[a][b][att_present] == board[i][j][att_present])
                    {
                       if (board[a][b][att_count] < board[i][j][att_count])
                       {
                          board[i][j][att_count]--;
                          change_count_a++;
                          paint_piece(i,j);
                       }
                    }
                }
                for (int i = 1; i <= 14; i++)
                for (int j = 1; j <= 14; j++)
                {
                    if (board[A][B][att_present] == board[i][j][att_present])
                    {
                       if (board[A][B][att_count] < board[i][j][att_count])
                       {
                          board[i][j][att_count]--;
                          change_count_A++;
                          paint_piece(i,j);
                       }
                    }
                }
                piece_count[board[a][b][att_present]] = board[a][b][att_count] + change_count_a;
                piece_count[board[A][B][att_present]] = board[A][B][att_count] + change_count_A;
                kill_piece(a,b);
                kill_piece(A,B);
          }
          else if ((board[a][b][att_health] <= board[A][B][att_attack] / 2) &&
                   (board[A][B][att_health] > board[a][b][att_attack])) //attacker dies.
          {
                for (int i = 1; i <= 14; i++)
                for (int j = 1; j <= 14; j++)
                {
                    if (board[a][b][att_present] == board[i][j][att_present])
                    {
                       if (board[a][b][att_count] < board[i][j][att_count])
                       {
                          board[i][j][att_count]--;
                          change_count_a++;
                          paint_piece(i,j);
                       }
                    }
                }
                board[A][B][att_health] -= board[a][b][att_attack];
                paint_piece(A,B);
                resource_tally(A,B);
                piece_count[board[a][b][att_present]] = board[a][b][att_count] + change_count_a;
                kill_piece(a,b);
          }
          else if ((board[a][b][att_health] > board[A][B][att_attack] / 2) &&
                   (board[A][B][att_health] <= board[a][b][att_attack])) //defender dies.
          {
                for (int i = 1; i <= 14; i++)
                for (int j = 1; j <= 14; j++)
                {
                    if (board[A][B][att_present] == board[i][j][att_present])
                    {
                       if (board[A][B][att_count] < board[i][j][att_count])
                       {
                          board[i][j][att_count]--;
                          change_count_A++;
                          paint_piece(i,j);
                       }
                    }
                }
                board[a][b][att_health] -= board[A][B][att_attack] / 2;
                piece_count[board[A][B][att_present]] = board[A][B][att_count] + change_count_A;
                kill_piece(A,B);
                move_ranged_piece(a,b,A,B);
                turn_count--;
          }
     }
     else if (a == A && b == B)
     {
          resource_tally(A,B);
     }
     else
     {
          display_text_xy(85,57,"Friendly Fire unavailable!");
          Sleep(1000);
          display_text_xy(85,57,"                          ");
          turn_count--;
     }
     turn_count++;
}

void kill_piece(int a, int b)
{
     piece_count[board[a][b][att_present]]--;
     erase_piece(a,b);
}

int turn_check()
{
    if (turn_count % 2 == 0)
       return my;
    else if (turn_count % 2 == 1)
       return his;
}

int my_victory_check()
{
    int victory_flag = 10;
    for (int i = 1; i <= 14; i++)
    for (int j = 1; j <= 14; j++)
    {
        if (board[i][j][att_present] == 8)
        {
                                     victory_flag = 0;
                                     return victory_flag;
        }
        else
        {
                         victory_flag = 1;
        }
    }
    return victory_flag;
}

int his_victory_check()
{
    int victory_flag = 10;
    for (int i = 1; i <= 14; i++)
    for (int j = 1; j <= 14; j++)
    {
        if (board[i][j][att_present] == 4)
        {
                                     victory_flag = 0;
                                     return victory_flag;
        }
        else
        {
                         victory_flag = 2;
        }
    }
    return victory_flag;
}


void clr_options(int who)
{
     if (who == my)
     {
             display_text_xy(80,35,"              ");
             display_text_xy(80,36,"               ");
             display_text_xy(80,37,"            ");
             display_text_xy(80,38,"         ");
     }
     else if (who == his)
     {
             display_text_xy(103,35,"              ");
             display_text_xy(103,36,"               ");
             display_text_xy(103,37,"            ");
             display_text_xy(103,38,"         ");
     }
}
